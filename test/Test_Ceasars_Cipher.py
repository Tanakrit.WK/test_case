from coe_number.Ceasars_Cipher import caesarCipher
import unittest

class caesar_cipher(unittest.TestCase):
    def test_give_abcde_if_defgh(self):
        s=['abcde']
        is_defgh=caesarCipher(s)
        self.assertEqual(is_defgh, 'defgh')

    def test_give_middle_Outz_is_okffng_Qwvb(self):
        s=['middle-Outz']
        is_okffng_Qwvb=caesarCipher(s)
        self.assertEqual(is_okffng_Qwvb, 'okffng-Qwvb')