from coe_number.Funny_String import funnyString
import unittest

class funnystr(unittest.TestCase):
    def test_give_a_b_d_e_is_funny(self):
        s=['a','b','d','e']
        is_funny=funnyString(s)
        self.assertEqual(is_funny, 'Funny')
    
    def test_give_a_c_x_z_is_funny(self):
        s=['a','c','x','z']
        is_funny=funnyString(s)
        self.assertEqual(is_funny, 'Funny')
    
    def test_give_a_b_d_e_is_funny(self):
        s=['a','b','d','x']
        is_notfunny=funnyString(s)
        self.assertEqual(is_notfunny, 'Not Funny')