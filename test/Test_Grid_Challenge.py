from coe_number.Grid_Challenge import gridChallenge
import unittest

class gridchall(unittest.TestCase):
    def test_give_1_5_eabcd_fghij_olkmn_trpqs_xywuv_is_YES(self):
        s=[['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']]
        is_defgh=gridChallenge(s)
        self.assertEqual(is_defgh, 'YES')