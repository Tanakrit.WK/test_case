from coe_number.Alternating_Charecter import alternatingCharacters
import unittest

class alterchar(unittest.TestCase):
    def test_give_AAAA_is_3(self):
        s=['AAAA']
        is_3=alternatingCharacters(s)
        self.assertEqual(is_3, '3')
    
    def test_give_BBBBB_is_4(self):
        s=['BBBBB']
        is_4=alternatingCharacters(s)
        self.assertEqual(is_4, '4')
    
    def test_give_ABABAB_is_0(self):
        s=['ABABAB']
        is_0=alternatingCharacters(s)
        self.assertEqual(is_0, '0')
